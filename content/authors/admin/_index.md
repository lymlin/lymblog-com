---
authors:
- Lingmin Lin
bio: My research interest is to find the master switch of stroke and to facilitate the rehabilitation process; and now is trying to investigate potential candidate targets for acute ischemic stroke with booming deep learning techniques.

education:
  courses:
  - course: Master in Neurology
    institution: Tianjin Medical University General Hospital, Tianjin Neurological Institute
    year: 2020
  - course: Bachelor in Clinical Medicine
    institution: Tianjin Medical University
    year: 2018
email: ""
interests:
- CNS Injure and Immune
- Bioinformatics
- Artificial Intelligence
name: Lingmin Lin
organizations:
- name: Tianjin Neurological Institute
  url: ""
- name: Tianjin Medical University General Hospital
  url: "http://www.tjmugh.com.cn/yyb.shtml"
role: Intern and Researcher
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/lymlin14
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=B8EoH1MAAAAJ&hl=en
- icon: github
  icon_pack: fab
  link: https://github.com/lymlin
superuser: true
user_groups:
- Researchers
- Visitors 
---

Lingmin Lin is an intern trained in the Tianjin Medical University General Hospital, who also chosed neurology and practiced laboratory skills in the Tianjin Neurological Institute. The research interest of Lin is to find the master switch of CNS disease like stroke, and to facilitate the rehabilitation process. As for now, it is to investigate potential targets for acute ischemic stroke with booming deep learning techniques.

On the way seeking life value and innermost desires step by step, Lin has practiced several projects with grants from university. At the meanwhile, some computer techniques and research principles have been developed, along with sense of mission to promote disease prognosis.
