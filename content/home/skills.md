+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Skills"
subtitle = ""

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons

#laboratory skills
[[feature]]
  icon = "user-md"
  icon_pack = "fas"
  name = "Clinical Practice"
  description = """70% \t
  Common diseases \t
  Clinical care \t
  """  

[[feature]]
  icon = "vials"
  icon_pack = "fas"
  name = "Lab Techniques"
  description = """70% \t 
  Animal management \t
  Histological techniques \t
  Cell isolation and culture"""
  
[[feature]]
  icon = "database"
  icon_pack = "fas"
  name = "Data Management"
  description = """40% \t
  Data processing \t
  Support application \t
  Work exhibition"""  
  

  
# Programming skills

[[feature]]
  icon = "r-project"
  icon_pack = "fab"
  name = "R"
  description = """50% \t 
  Deep learning in diabetes \t
  Bioinformatics
"""
[[feature]]
  icon = "ubuntu"
  icon_pack = "fab"
  name = "Linux"
  description = """20% \t
  Deep learning in diabetes \t
  Bioinformatics"""

[[feature]]
  icon = "python"
  icon_pack = "fab"
  name = "Pytorch"
  description = """10% \t
  Deep learning practice"""

# Group and personality


[[feature]]
  icon = "users"
  icon_pack = "fas"
  name = "Group"
  description = """70% \t
  Cooperation \t
  Encouraging \t
  Leadership 
  """

[[feature]]
  icon = "user-circle"
  icon_pack = "fas"
  name = "Portrait"
  description = """60% \t
  Independence \t
  Self-learning \t
  Creditability
  """

[[feature]]
  icon = "chalkboard-teacher"
  icon_pack = "fas"
  name = "Teaching"
  description = """50% \t
  Home tutoring for 2 \t 
  senior-high-school students
  """
+++
