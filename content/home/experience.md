+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Experience"
#subtitle = "Research Experience"

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Neurological Training"
  company = "Tianjin Medical University General Hospital"
  company_url = ""
  location = "Tianjin"
  date_start = "2017-08-01"
  date_end = ""
  description = """

##### Doctor Junwei Hao
National Distinguished Young \t
Tianjin Neurological Institute \t
Tianjin Medical University General Hospital

###### Tasks:
**Research Training**

* Research assistant in the cGAS research for multiple sclerosis project
* Research assistant in the scRNA research for stroke project
* Masters degree research project：Deep learning in marker investigation of stroke

---
**Skills**

* Developed in-depth knowledge of literature searching and bioinformatics analysis, like differential analysis and gene annotation
* Practiced basic biological and immunological techniques such as PCR,  immunofluorescent staining and cell separation techniques
"""


[[experience]]
  title = "Clinical Training"
  company = """ 
  Tianjin Medical University General Hospital
 """
  company_url = ""
  location = "Tianjin"
  date_start = "2017-07-01"
  date_end = ""
  description = """

###### Tasks:
**Clinical Training**

* 2019 Mar – Present	**Specialized training in neurology**
* 2018 Sep – 2019 Feb	**Intensified internship in general physics and surgery departments**
* 2017 Jul – 2018 Jul	**General internship, clinical rotation in various departments**

---
**Skills**

* Basic clinical practice skills
"""


[[experience]]
  title = "Computer Translational Training"
  company = """ 
  Tianjin Medical University
 """
  company_url = ""
  location = "Tianjin"
  date_start = "2017-05-01"
  date_end = "2019-05-01"
  description = """

##### Professor Jiarui Si
Computer Department, School of Basic Medical Sciences

##### Doctor Jing Li
Tianjin Medical University Metabolic Diseases Hospital

 
###### Tasks:

  2017-2019 **Diabetes glucose prediction and health management based on deep learning**.*The leader in the team*. \t
  Acted in: Research assistant, Protocol design, Task assignment, Medical translation.

---
**Skills**

* Developed several computer techniques: R, Linux, and a little Python
* Learned some frameworks of deep learning in Pytorch, like CNN and LSTM
* Practiced expression skills by oral presentation and academic writing
* Evolved cooperation skills in a group

  """



[[experience]]
  title = "Research Training"
  company = """ 
  Tianjin Medical University
 """
  company_url = ""
  location = "Tianjin"
  date_start = "2014-09-01"
  date_end = "2016-07-01"
  description = """
  
##### Professor Ping Zhang
 Department of Anatomy and Histology, School of Basic Medical Sciences


###### Tasks:

  2015 – 2016 **Risk assessment of lumbar disc herniation and exercise rehabilitation**. *The leader in the team*. \t
  Acted in: Played a role in protocol design, task assignment, data process and analysis.

  2015 Summer **Spinal health status survey for college students and healthy spine propaganda**.*The leader in the team*. \t
  Acted in: Cooperated in task assignment, questionnaire design and data analysis.

  2014 – 2015 **Experimental study for the inhibitory effect of Salubrinal on osteoclast cell lines**. \t
  Acted in: Research assistant, performed a simple drug-control experiment shown increased activation of osteoclast in osteoporosis ovariectomy mice.

---
**Skills**

* Developed writing skills to apply for funds and to compose research reports
* Practiced basic histological techniques, like paraffin slicing and staining
* Evolved cooperation and presenting skills in a group

  """

+++
